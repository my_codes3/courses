## Packages used
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

## Global variables
LL = np.array([4,5,6]) # Sizes array
n_sizesL = LL.size

##### crtitical beta:
beta_c=1/1.44
beta_arr=1/np.linspace(1.45,1.46,8)
n_betas=np.size(beta_arr)

# some functions:
def ransamplep(L): # projection sampling, to sample uniformly
    vec=np.zeros((L,L,L,3))

    vec[:,:,:,1] = 2*np.pi*np.random.rand(L,L,L)
    vec[:,:,:,2] = 2*np.random.rand(L,L,L)-1

    vec[:,:,:,0] = np.sqrt(1-vec[:,:,:,2]**2)*np.cos(vec[:,:,:,1])
    vec[:,:,:,1] = np.sqrt(1-vec[:,:,:,2]**2)*np.sin(vec[:,:,:,1])

    return vec

def total_energy(x,L):
    energy = 0
    # parameter L can be used to compute energy only within a L^3 cube
    for i in range(L):
        for j in range(L):
            for k in range(L):
                energy -= np.dot((x[(i-1)%L,j,k,:] + x[i,(j-1)%L,k,:] + x[i,j,(k-1)%L,:]),x[i,j,k,:])

    return energy

def move(x, M, E, beta):
    """
    Args:
        x: Spin configuration
        M: Magnetization of x
        E: Energy of x
        beta: beta value
    Returns:
        Updated x, M and E after 1 Monte Carlo move
    """
    # pick one site at random
    site = np.random.randint(0,L**3)
    k = int(site % L)
    j = int((site // L) % L)
    i = int(site // (L**2))

    # pick random unit vector
    newx=ransamplep(1)[0,0,0,:]

    # compute the local magnetic field at site (i,j,k) due to nearest-neighbours
    nn_sum = x[(i-1)%L,j,k,:] + x[(i+1)%L,j,k,:] + x[i,(j-1)%L,k,:] + x[i,(j+1)%L,k,:] + x[i,j,(k-1)%L,:] + x[i,j,(k+1)%L,:]

    # energy change if swapped
    dE = np.dot(nn_sum, (x[i,j,k,:]-newx))

    # Flip the spin of that site according to the Metropolis algorithm
    if dE <= 0 or np.random.rand() < np.exp(-dE*beta):
        # flip the spin
        M -= x[i,j,k,:]
        x[i,j,k,:] = newx
        # update the magnetisation and energy
        E += dE
        M += x[i,j,k,:]

    return x, M, E

# main:
for lcounter in range(LL.size):
    L = LL[lcounter]
    N = L**3

    m_arr = np.zeros(n_betas) # average magnetizations per site
    e_arr = np.zeros(n_betas) # average energies per site
    chi_arr = np.zeros(n_betas) # magnetic susceptibilities
    cv_arr = np.zeros(n_betas) # heat capacities

    # Initial configuration
    x = ransamplep(L)
    binder = np.zeros(n_betas)
    for b in range(n_betas):

        # can be adapted to be function of (L,beta):
        Nthermalization=int(1e5) # number of thermalization steps
        Nsample = 512 # number of samples (= size of the Markov chain)
        Nsubsweep = 3*N # number of subsweeps (to generate better samples)

        beta = beta_arr[b]
        print('Running for inverse temperature = ',beta, 'i.e. for T=', 1/beta)
        m_data=np.zeros(Nsample)
        e_data = np.zeros(Nsample)
        # compute its initial magnetisation and energy
        M = np.sum(x,axis=(0,1,2))
        E = total_energy(x,L)

        # run a thermalisation loop
        for i in range(1,Nthermalization):
            x, M, E = move(x, M, E, beta)

        # measurement of M and E
        m_data[0]= np.linalg.norm(M)/N
        e_data[0] = E/N

        # sampling
        for n in tqdm(range(1, Nsample)):
            for k in range(Nsubsweep):
                x, M, E = move(x, M, E,beta)
            e_data[n] = E/N
            m_data[n] = np.linalg.norm(M)/N

        binder[b]= 1 - np.mean(m_data**4)/(3*(np.mean(m_data**2))**2)
        m_arr[b] = np.mean(m_data) # average magnetization per site
        e_arr[b] = np.mean(e_data) # average energy per site

        cv_arr[b]=N*(beta**2)*np.var(e_data)
        chi_arr[b]=N*(beta)*np.var(m_data)
    np.savetxt('binder_L%s.txt' %L, np.column_stack([1/beta_arr, binder]))
    np.savetxt('L%s.txt' %L, np.column_stack([1/beta_arr, m_arr, e_arr, cv_arr, chi_arr]))



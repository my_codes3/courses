import numpy as np
import matplotlib.pyplot as plt
import time
from copy import deepcopy
from tqdm import tqdm

np.set_printoptions(precision=20)
#1d code

#Hard spheres in 1d simulated with event-driven molecular dynamics

def energy(v):
    E=0
    for i in range(N):
        E += m*(v[i]**2)/2
    return E

def find_t(x,v):
    #print(v)
    t=np.zeros(N+1)
    a=-1
    b=-10
    t[0]=-x[0]/v[0]
    if a<0 and t[0]>0:
        a=t[0]
        b=-1     #it determines the particle which will interact first
    t[N]=(L-x[N-1])/v[N-1]
    if (a<0 and t[N]>0)or(t[N]>0 and t[N]<a):
        a=t[N]
        b=N
    for i in range(N-1):
        t[i+1]=(x[i+1]-x[i])/(v[i]-v[i+1])
        if (a<0 and t[i+1]>0)or(t[i+1]>0 and t[i+1]<a):
            a=t[i+1]
            b=i

    return t, a, b

L=10
m=1
er=0.7      #restitution coefficient
N=13
#x=np.zeros(N)
x= np.random.rand(N)*L
x=np.sort(x)
#v=np.zeros(N)
v=np.random.normal(size=N)*(L/2)
t=np.zeros(N+1)
S=1000
real_time=np.zeros(S)
energy_arr=np.zeros(S+1)
energy_arr[0]=energy(v)

for s in tqdm(range(S)):
    t, a , b=find_t(x, v)
    #print('x=')
    #print(x, flush=True)
    #print('v=')
    #print(v, flush=True)
    #print(a, flush=True)
    x += v*a
    if b==-1:
        v[0] = -er*v[0]
    elif b==N:
        v[N-1] = -er*v[N-1]
    else:
        u=v[b]-v[b+1]
        v[b] = v[b] - (1+er)*u/2
        v[b+1] = v[b+1] + (1+er)*u/2
    energy_arr[s+1]=energy(v)
    real_time[s]=a
    for i in range(N-1):
        if x[i+1]<x[i]:
            xtmp=x[i]
            x[i]=x[i+1]
            x[i+1]=xtmp
            vtmp=v[i]
            v[i]=v[i+1]
            v[i+1]=vtmp
    if x[0]<0:
        x[0]=0
    if x[N-1]>L:
        x[N-1]=L



plt.figure(1)
plt.plot(energy_arr)
plt.ylim(0, 1.1*np.max(energy_arr))
plt.xlabel('step')
plt.ylabel('Energy')
#plt.savefig('Energy_dte_6.pdf')
#plt.show()

plt.figure(2)
plt.plot(real_time)
plt.ylim(0.9*np.min(real_time), 1.1*np.max(real_time))
plt.xlabel('steps')
plt.ylabel('timestep')

plt.show()


import numpy as np
from dataclasses import dataclass

@dataclass # this seems like a good tool for the job! -Juan
class site:
    x: int
    y: int
    z: int

def total_energy(x):
    energy = 0
    # compute energy of the configuration x.
    # consider only up and left bonds of each spin to avoid double counting.
    for i in range(L):
        for j in range(L):
            for k in range(L):
                energy -= (x[(i-1)%L,j,k] + x[i,(j-1)%L,k] + x[i,j,(k-1)%L])*x[i,j,k]

    return energy

def random_site():
    # pick one spin at random
    site = np.random.randint(0,L**3)
    k = int(site % L)
    j = int((site // L) % L)
    i = int(site // (L**2))

    return i, j, k



def move(latx, index):
    """
    One step of the Wolff algorithm
    Make a cluster of links with p = 1 - exp(-2 beta J)

    Args:

    Returns:

    """
    i, j, k = random_site() # pick one spin at random

    latx[i,j,k]=-latx[i,j,k]         #I flip the spins in this function, avoiding further for # I think it's still the same number of operations if we do it at the end, we can check later
    laty=np.zeros((L,L,L))      # actually, I think it's better to save the lattice sites on a list by checking == and then flip the whole cluster because that way it can easily be addapted to potts
    laty[i,j,k]=1             #1 if connected, 0 otherwise
    p=site(i, j, k)
    check=[]
    check.append(p) #good
    while len(check):
        p=check[0]
        x1 = p.x
        y1 = p.y
        z1 = p.z
        check.remove(site(p.x,p.y,p.z))


        if laty[(x1+1)%L, y1, z1]==0:
            if latx[(x1+1)%L, y1, z1]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[(x1+1)%L, y1, z1]=1
                    latx[(x1+1)%L, y1, z1]=-latx[(x1+1)%L, y1, z1]
                    p=site((x1+1)%L, y1, z1)
                    check.append(p)
        if laty[(x1-1)%L, y1, z1]==0:
            if latx[(x1-1)%L, y1, z1]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[(x1-1)%L, y1, z1]=1
                    latx[(x1-1)%L, y1, z1]=-latx[(x1-1)%L, y1, z1]
                    p=site((x1-1)%L, y1, z1)
                    check.append(p)

        if laty[x1, (y1+1)%L, z1]==0:
            if latx[x1, (y1+1)%L, z1]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[x1, (y1+1)%L, z1]=1
                    latx[x1, (y1+1)%L, z1]=-latx[x1, (y1+1)%L, z1]
                    p=site(x1, (y1+1)%L, z1)
                    check.append(p)
        if laty[x1, (y1-1)%L, z1]==0:
            if latx[x1, (y1-1)%L, z1]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[x1, (y1-1)%L, z1]=1
                    latx[x1, (y1-1)%L, z1]=-latx[x1, (y1-1)%L, z1]
                    p=site(x1, (y1-1)%L, z1)
                    check.append(p)

        if laty[x1, y1, (z1+1)%L]==0:
            if latx[x1, y1, (z1+1)%L]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[x1, y1, (z1+1)%L]=1
                    latx[x1, y1, (z1+1)%L]=-latx[x1, y1, (z1+1)%L]
                    p=site(x1, y1, (z1+1)%L)
                    check.append(p)
        if laty[x1, y1, (z1-1)%L]==0:
            if latx[x1, y1, (z1-1)%L]!=latx[x1, y1, z1]:
                a=np.random.rand()
                if a<prob[index]:
                    laty[x1, y1, (z1-1)%L]=1
                    latx[x1, y1, (z1-1)%L]=-latx[x1, y1, (z1-1)%L]
                    p=site(x1, y1, (z1-1)%L)
                    check.append(p)

    return latx



 

L = 8 # linear size of the system
N = L**3 # total system size
#J = 1  coupling constant
Nsample = 5000#0 # number of samples
Nsubsweep = 3#*N # number of subsweeps (3 is the safety factor)


#cv_arr = np.zeros(Nsample) # heat capacities
beta = np.linspace(.15,.30,5) #temperatures
E_arr = np.zeros(beta.size) # average energies
E_err = np.zeros(beta.size) # standard deviations of the energies
prob=1-np.exp(-2*J*beta)
for e in range(beta.size):
    # start with uniform initial configuration
    latx = np.ones((L, L, L))

    E = total_energy(latx) # every lattice site contributes an energy -3J

    #print('Energising the system...')
    # bring the system to the specified energy
    '''
    while Esys_arr[e] > E:
      latx, M, E = increase_energy(x, M, E)
    print('System reached the specified energy', Esys_arr[e], '!')
    '''
    # initialise arrays holding magnetisation and energy densities
    # M_data = np.zeros(Nsample)
    E_data = np.zeros(Nsample)

    print('Running the Monte Carlo simulation at energy...')
    #M_data[0] = np.abs(M)/N # initial magnetisation density
    E_data[0] = E/N # initial energy density

    for n in range(1, Nsample):
        for t in range(Nsubsweep):
            latx = move(latx, e)

        #Ed_arr[n] = Ed # save the demon energies
        # M_data[n] = np.abs(M)/N
        E_data[n] = total_energy(latx)/N

    #M_arr[e] = np.mean(M_data) # average magnetization
    E_arr[e] = np.mean(E_data) # average energy
    #print(E_data[])
    #M_err[e] = np.std(M_data) # standard deviation of the magnetization
    E_err[e] = np.std(E_data) # standard deviation of the energy



    #beta[e]=(1/6)*np.log(1+6/np.mean(Ed_arr))

out_dir="wolf_"
    #print('The estimated temperature of system at energy ', Esys_arr[e], ' is ', T_arr[e])  out_dir = 'outputs/'
# np.savetxt(out_dir+'T'+str(L)+'.txt', T_arr, newline='\r\n')
# np.savetxt(out_dir+'M'+str(L)+'.txt', M_arr, newline='\r\n')
np.savetxt(out_dir+'E'+str(L)+'.txt', E_arr, newline='\r\n')
# np.savetxt(out_dir+'M_err'+str(L)+'.txt', M_err, newline='\r\n')
np.savetxt(out_dir+'E_err'+str(L)+'.txt', E_err, newline='\r\n')

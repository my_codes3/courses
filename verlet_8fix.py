import os
import vtktools
import numpy as np
import matplotlib.pyplot as plt
import time
from copy import deepcopy
from tqdm import tqdm


data_dir = "simu"
if not os.path.exists(data_dir):
    os.makedirs(data_dir)



def r_rel_pbc(ri, rj):
    """
    Compute the relative position of particles i and j in a box
    with periodic boundary conditions.

    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        the shortest relative distance with correct orientation
        between particles i and j in periodic boundary conditions (PBC)
    """
    r_vec = rj - ri # relative distance without PBC

    for k in range(3):
        r_k = rj[k]-ri[k]
        if abs(r_k) > L/2.0:
            r_vec[k] = -np.sign(r_k)*(L - abs(r_k))

    return r_vec


def potential(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) potential energy between particles
    """
    
    rv=r_rel_pbc(ri,rj)
    r = np.dot(rv, rv)
    if r < rc2:
        return (4*(r**(-6)-r**(-3))- 4*(rc2**(-6)-rc2**(-3))+24*(2*(rc2**(-6))/rc - (rc2**(-3))/rc)*(np.sqrt(r)-rc))
    else:
        return 0.






def force(ri, rj):
    """
    Args:
        ri: Position vector of particle i
        rj: Position vector of particle j (i must not be equal to j)

    Returns:
        (Lennard-Jones) force vector
    """
    
    r_vec = r_rel_pbc(ri,rj)
    r = np.dot(r_vec, r_vec)
    if r < rc2:
        return -4*(12*r**(-7)-6*r**(-4))*r_vec + 24*(2*(rc2**(-6))/rc - (rc2**(-3))/rc)*r_vec/np.sqrt(r)
    else:
        return 0*r_vec

def potential_neigh(r_current, a, i, first, listar):
    #potential due to particles in neighbouring cells
    p0=0
    L0=i%siteL
    L1=((i-L0)/siteL)%siteL
    L2=(i-L0-L1*siteL)/(siteL**2)
    neigh=np.zeros(13)   #neighbouring cells (only the part to avoid double counting)
    neigh=neigh.astype(int)
    neigh[0]=(L0+1)%siteL +L1*siteL + L2*siteL**2
    for l in range(3):
        neigh[1+l]=(L0-1+l)%siteL + ((L1+1)%siteL)*siteL + L2*siteL**2
    for l in range(3):
        neigh[4+l]=(L0-1+l)%siteL + ((L1-1)%siteL)*siteL + ((L2+1)%siteL)*siteL**2
    for l in range(3):
        neigh[7+l]=(L0-1+l)%siteL + L1*siteL + ((L2+1)%siteL)*siteL**2
    for l in range(3):
        neigh[10+l]=(L0-1+l)%siteL + ((L1+1)%siteL)*siteL + ((L2+1)%siteL)*siteL**2
    #print('qua ok')
    for l in range(13):
        #print(l)
        b=first[neigh[l]]
        while b!=-1:
            #print(b)
            p0 +=potential(r_current[a,:], r_current[b,:])
            b=listar[b]
            #if b==listar[b]:
                #print('ERRORE')
    return p0

def rec_potential(r_current, a,i, first, listar):
    #potential due to particles in the same cell
    p1=0
    if listar[a]!=-1:
        b=listar[a]
        p1=potential_neigh(r_current, b,i, first, listar)
        while(b!=-1):
            p1 += potential(r_current[a,:], r_current[b,:])
            b=listar[b]
        p1 += rec_potential(r_current, listar[a], i, first, listar)
        return p1
    else:
        return p1
    return p1

def energy(r_current, v_current, first, listar):
    """
    Args:
        r_current: Current particle positions
        v_current: Current particle velocities

    Returns:
        Total energy of the system
    """
   .
    E_kin = ((np.linalg.norm(v_current))**2)/(2*m)

   
    E_pot = 0.

    for i in range(siteL**3):
        #print(i)
        if first[i]==-1:
            continue
        else:
            #print('sono in else')
            a=first[i]
            E_pot += potential_neigh(r_current, a, i, first, listar)
            #print('E_pot_neigh ok')
            E_pot += rec_potential(r_current, a, i, first, listar)

    # E_pot = E_pot/2

    return E_kin + E_pot

def force_neigh(r_current, a, i, F, first, listar):
    L0=i%siteL
    L1=((i-L0)/siteL)%siteL
    L2=(i-L0-L1*siteL)/(siteL**2)
    neigh=np.zeros(13)   #neighbouring cells (only the part to avoid double counting)
    neigh=neigh.astype(int)
    neigh[0]=(L0+1)%siteL +L1*siteL + L2*siteL**2
    for l in range(3):
        neigh[1+l]=(L0+-1 +l)%siteL + ((L1+1)%siteL)*siteL + L2*siteL**2
    for l in range(3):
        neigh[4+l]=(L0-1+l)%siteL + ((L1-1)%siteL)*siteL + ((L2+1)%siteL)*siteL**2
    for l in range(3):
        neigh[7+l]=(L0-1+l)%siteL + L1*siteL + ((L2+1)%siteL)*siteL**2
    for l in range(3):
        neigh[10+l]=(L0-1+l)%siteL + ((L1+1)%siteL)*siteL + ((L2+1)%siteL)*siteL**2



    for l in range(13):
        #if neigh[l]==i:
        #    print('same cell, l=%d, i=%d' %(l, i))
        b=first[neigh[l]]
        while b!=-1:
            #if a==b:
            #    print('a=b in force_neigh')
            F[a,b,:]=force(r_current[a,:], r_current[b,:])
            F[b,a,:]=-F[a,b,:]
            b=listar[b]
            #print(b)
    return 0




def rec_force(r_current, a,i, F, first, listar):
    #force due to particles in the same cell
    if listar[a]!=-1:
        b=listar[a]
        force_neigh(r_current, b,i, F, first, listar)
        while(b!=-1):
            if a==b:
                print('a=b in rec_force')
            F[a,b,:]=force(r_current[a,:], r_current[b,:])
            F[b,a,:]=-F[a,b,:]
            b=listar[b]
        #print('beginning a new rec_force')
        rec_force(r_current, listar[a], i, F, first, listar)
        #print('esco da rec_force')
        return -1
    else:
        return -1
    return -1



def stepVerlet(r_previous, r_current, first, listar):
    """
    Args:
        r_previous: Particle positions at time t-dt
        r_current: Particle positions at time t

    Returns:
        Updated positions as well as velocities and forces according to the
        Verlet scheme
    """

    # if the Verlet step drifts the particle outside the box
    # restore the particle into the box according to PBC
    # r_current_pbc = r_current%L

    F = np.zeros((N,N,3))
    
    for i in range(siteL**3):
        #print(i)
        if first[i]==-1:
            continue
        else:
            #print(i)
            a=first[i]
            force_neigh(r_current, a, i, F, first, listar)
           # print('force neigh ok')
            rec_force(r_current, a, i, F, first, listar)

    #print('forza fatta')

    F = F.sum(axis=1)    #Gabriele changed here

    r_next = np.zeros((N, 3)) # positions after the Verlet step
    del_r = np.zeros((N, 3)) # position changes between two Verlet steps


    for i in range(N):
        
        r_next[i,:] = 2*r_current[i,:] - r_previous[i,:] + dt**2 * F[i,:]/m
        del_r[i, :] = r_next[i, :] - r_previous[i, :]

        r_next = r_next%L
        # if any(r_current[i,:] != r_current_pbc[i,:]):
        


    
    v_current = del_r/(2*dt)

    #print('step fatto')

    return r_current, v_current, r_next, F



"""
Parameters
"""
N = 30 # particle number
L = 20 # box length
rc = 2.5 # cutoff-length
rc2=rc**2
timemont = .01
dt = 10**-6 # time step
T = int(timemont/dt) # simulation steps
m = 1
first=np.zeros(L**3)
first=first.astype(int)
first[:]=-1
listar=np.zeros(N)
listar=listar.astype(int)
listar[:]=-1
firstnext=np.zeros(L**3)
firstnext=firstnext.astype(int)
firstnext[:]=-1
listarnext=np.zeros(N)
listarnext=listarnext.astype(int)
listarnext[:]=-1
lsite=2*rc      #length of a cell
siteL=np.int(np.floor(L/(2*rc)))
inizio =time.time()
"""
Initialization
"""
energy_arr = np.zeros(T)


# by sampling `N` particles inside the cubic box of volume L**3, centred at (L,L,L)/2.
r_current = np.random.rand(N,3)*2*L

for i in range(N):
    a=np.int((r_current[i,0]-r_current[i,0]%lsite)/lsite)
    b=np.int((r_current[i,1]-r_current[i,1]%lsite)/lsite)
    c=np.int((r_current[i,2]-r_current[i,2]%lsite)/lsite)
    nlattice=a+b*siteL+c*(siteL**2)
    if first[nlattice]==-1:
        first[nlattice]=i
    else:
        listar[i]=first[nlattice]
        first[nlattice]=i
        #if listar[i]==i:
         #   print('problem first')


v_current = np.random.normal(size=(N,3))*L/10

r_next = r_current + v_current*dt # particle positions at time t0+dt


# Run the time evolution for `T` steps:
vtk_writer = vtktools.VTK_XML_Serial_Unstructured()
for t in tqdm(range(T)):
    r_current, v_current, r_next, F_ij = stepVerlet(r_current, r_next, first, listar)

    first[:]=-1
    listar[:]=-1

    for i in range(N):
        a=np.int((r_current[i,0]-r_current[i,0]%lsite)/lsite)
        b=np.int((r_current[i,1]-r_current[i,1]%lsite)/lsite)
        c=np.int((r_current[i,2]-r_current[i,2]%lsite)/lsite)
        nlattice=a+b*siteL+c*(siteL**2)
        if first[nlattice]==-1:
            first[nlattice]=i
        else:
            listar[i]=first[nlattice]
            first[nlattice]=i
            #if listar[i]==i:
                #print('problem second')

    firstnext[:]=-1
    listarnext[:]=-1
    for i in range(N):
        a=np.int((r_next[i,0]-r_next[i,0]%lsite)/lsite)
        b=np.int((r_next[i,1]-r_next[i,1]%lsite)/lsite)
        c=np.int((r_next[i,2]-r_next[i,2]%lsite)/lsite)
        nlattice=a+b*siteL+c*(siteL**2)
        if firstnext[nlattice]==-1:
            firstnext[nlattice]=i
        else:
            listarnext[i]=firstnext[nlattice]
            firstnext[nlattice]=i
            #if listar[i]==i:
                #print('problem second')


    #print('prima di energia')
    energy_arr[t] = energy(r_next, v_current, firstnext, listarnext)     #here I put r_current
    #print('dopo energia')

    r_current = r_current%L
    r_x = r_current[:, 0]
    r_y = r_current[:, 1]
    r_z = r_current[:, 2]
    F_x = F_ij[:, 0]
    F_y = F_ij[:, 1]
    F_z = F_ij[:, 2]
    vtk_writer.snapshot(os.path.join(data_dir, "MD"+str(t)+".vtu"), r_x, r_y, r_z, \
        x_force=F_x, y_force=F_y, z_force=F_z)


fine=time.time()
vtk_writer.writePVD(os.path.join(data_dir, "MD.pvd"))

E=np.mean(energy_arr)
print(E)
#energy_new=[x for x in energy_arr if x<1000000]
#E=np.mean(energy_new)
Evar=np.std(energy_arr)/np.sqrt(np.size(energy_arr)-1)

np.savetxt('energy_dte_6.txt', np.column_stack([E, Evar]))
np.savetxt('time_steps_30.txt', np.column_stack([fine-inizio, N]))
"""
Plotting the system energy
"""
plt.figure()
plt.plot(energy_arr)
plt.ylim(0, 1.1*np.max(energy_arr))
plt.xlabel('Timesteps')
plt.ylabel('Energy')
plt.savefig('Energy_dte_6.pdf')
plt.show()
